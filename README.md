# Image Meta Cleaner:

Image Meta Cleaner is a cross-platform application designed to remove metadata from images, ensuring user privacy and security. Metadata often contains sensitive information such as geolocation, device information, and timestamps. By removing this metadata, Image Meta Cleaner helps users protect their privacy when sharing images online.

## Features:

- **Metadata Removal**: Easily remove metadata from images with just a few taps.
- **Platform Compatibility**: Image Meta Cleaner can run on Linux, Windows, macOS, iOS, and Android devices.
- **User-Friendly Interface**: Intuitive interface makes it easy for users to remove metadata from their images.
- **View Metadata**: Gain insights into the metadata embedded in your images, including geolocation, device information, and timestamps, allowing you to make informed decisions about privacy.
- **Share Cleaned Images**: Share your images with confidence after removing sensitive metadata, ensuring your privacy is preserved while sharing with friends, family, or on social media platforms.

## Getting Started:

### Prerequisites:

- Flutter SDK installed on your development environment.
- Android Studio or Xcode for running the app on an emulator or physical device.

### Installation:

1. Clone the repository to your local machine:

   ```bash
   git clone https://notabug.org/alimiracle/image_meta_cleaner
```

2. Navigate to the project directory:

```bash
cd image_meta_cleaner
```

3. Run the following command to install dependencies:

```bash
flutter pub get
```

4. Run the app on an emulator or physical device:

```bash
flutter run
```

5. To build a release version of the app, run the following commands:

For Android (APK):

```bash
flutter build apk
```

For iOS (IPA):

```bash
flutter build ios
```

for windos:

```bash
flutter build windows
```

For Linux:

```bash
flutter build linux
```

For macOS:

```bash
flutter build macos
```

The built Files will be located in the `build` directory of your project, depending on the platform you targeted.

### Usage:

1. **Open Image Meta Cleaner**: Launch the Image Meta Cleaner app on your device.
2. **Select Image**: Tap the "Pick Image" button to choose an image from your device's gallery.
3. **Remove Metadata**: Once the image is selected, tap the "Remove Metadata" button to initiate the removal of metadata from the image.
4. **Save Cleaned Image**: The cleaned image, without any metadata, will be automatically saved to your device's gallery.

With Image Meta Cleaner, you can easily ensure the privacy and security of your images by removing sensitive metadata before sharing them online.

## Contributing:

We welcome contributions from the community! If you encounter any bugs, have feature requests, or would like to contribute enhancements, please follow these steps:

1. **Fork** the repository on GitHub.
2. **Clone** your forked repository to your local machine.
   ```bash
https://notabug.org/<yourusername>/image_meta_cleaner
```

## License:

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details.
