import 'package:flutter/material.dart';
import 'about_page.dart';
import 'remove_metadata_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Image Meta Cleaner',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => const RemoveMetadataPage(),
        '/about': (context) => const AboutPage(), // Add this route for the About page
      },
    );
  }
}
